<!DOCTYPE html>
<html>
<head>
<title>PENDAFTARAN SISWA BARU - SD Unggulan Muhammadiyah Kretek</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" 
	integrity="9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIh0egiu1Fw05qRGvFX0dJZ4" crossorigin="anonymous">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial;
  padding: 1px;
  background: #f1f1f1;
}

/* Header/Blog Title */
.header {
  padding: 30px;
  text-align: center;
  background: white;
  background-image: url("Header2.png");
}

.header h1 {
  font-size: 50px;
}

.menu {
    font-family:arial;
    font-weight:bold;
    width:980;
    border-radius:5px;
    font-size:14px;
	background:#222;
}
.menu ul {
    padding:0;
    margin:0;
}
.menu ul li {
    list-style:none;
    display:inline-block;
}
.menu ul li a {
    display:block;
    text-decoration:none;
    padding:20px;
    color:white;
}
.menu ul li a:hover {
    background-color:#ddd;
    color:black;
}

.menu ul li .submenu {
    display:none;
}
.menu ul li:hover .submenu {
    display:block;
    position:absolute;
    background:linear-gradient(top, #333, #111);
    background:-webkit-linear-gradient(top, #333, #111);
    background:-moz-linear-gradient(top, #333, #111);
    padding:10px;
}
.menu ul li:hover .submenu li {
    display:block;
}
.menu ul li:hover .submenu li a{
    padding:10px;
    border-radius:5px;
    margin-bottom:5px;
}
.menu ul li:hover .submenu li a:hover {
    background:white;
    box-shadow:inset 0px 0px 5px #000;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  background-color: #f1f1f1;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #f1f1f1;
  width: 100%;
  padding: 20px;
}

/* Add a card effect for articles */
.card {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background-color: white;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
  .topnav a {
    float: none;
    width: 100%;
  }
}

* {box-sizing:border-box}
body {font-family: Verdana,sans-serif;}
.mySlides {display:none}
/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}
/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}
/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}
/* The dots/bullets/indicators */
.dot {
  height: 13px;
  width: 13px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}
.active {
  background-color: #717171;
}
/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}
@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>
</head>
<body>

<div class="header">
  <h1>PENDAFTARAN SISWA BARU</h1>
  <h2>SD Unggulan Muhammadiyah Kretek</h2>
</div>

<nav class="menu">
    <ul>
        <li><a href="index.php">HOME</a></li>
        <li><a href="daftarsiswa.php">Pendaftaran Siswa Baru</a></li>
		<li><a href="admin/fileupload.php">Unggah Kelengkapan Dokumen</a></li>
		<li><a href="lihatdaftarsiswa.php">Pengumuman Lulus Verifikasi</a></li>
		<li><a href="lihatlulususm.php">Pengumuman Lulus USM</a></li>
    </ul>
</nav>

<div class="row">
  <div class="leftcolumn">
    <div class="card">
	  <h1>Registrasi disini!</h1>
 <p><strong>Lengkapi formulir pendaftaran siswa</strong> dibawah ini, Setelah diisi  <strong>Mohon Dilihat Kembali</strong> lalu klik <strong>Daftar</strong> untuk registrasi.</p>
 <form action="#" method="post" class="form-horizontal" name="fregist" >
	<table>
	<tr>
			<td>NIK*</td>
			<td><input type="text" name="id" id="id" class="form-control" placeholder="(no induk siswa nasional)"></td>
	</tr>
    <tr>
			<td>Nama Lengkap Siswa*</td>
			<td><input type="text" name="nama" id="nama" class="form-control" placeholder="nama lengkap siswa"></td>
	</tr>
	<tr>
            <td>Jenis Kelamin*</td>
			<td><input name="jenis" type="radio" value="pria" />Pria
                <input name="jenis" type="radio" value="wanita" />Wanita
			</td>
    </tr>
	<tr>
			<td>Tempat*</td>
			<td>
				<input type="text" name="tempat" id="tempat" class="form-control" placeholder="tempat lahir">
			</td>
			<td>Tanggal Lahir*</td>
			<td>
				<input type="date" name="tgl" id="tgl" class="form-control">
			</td>
	</tr>
	<tr>
			<td>Anak Ke*</td>
			<td>
				<input type="text" name="anak" id="anak" class="form-control" placeholder="1/2/3/4/5/6 ...">
			</td>
			<td>Jumlah Saudara*</td>
			<td>
				<input type="text" name="jumlah" id="jumlah" class="form-control" placeholder="1/2/3/4/5/6 ...">
			</td>
	</tr>
	<tr>
			<td>Alamat*</td>
			<td>
				<input type="text" name="alamat" id="alamat" class="form-control" placeholder="alamat lengkap">
			</td>
			<td>RT*</td>
			<td>
				<input type="text" name="rt" id="rt" class="form-control" placeholder="01/02/03...">
			</td>
			<td>RW*</td>
			<td>
				<input type="text" name="rw" id="rw" class="form-control" placeholder="01/02/03...">
			</td>
	</tr>
	<tr>
			<td>Kelurahan / Desa*</td>
			<td>
				<input type="text" name="keldes" id="keldes" class="form-control" placeholder="kelurahan / desa">
			</td>
			<td>Kode pos*</td>
			<td>
				<input type="text" name="kodepos" id="kodepos" class="form-control" placeholder="kode pos">
			</td>
	</tr>
	<tr>
			<td>Kecamatan*</td>
			<td><input type="text" name="kec" id="kec" class="form-control" placeholder="kecamatan"></td>
	</tr>
	<tr>
			<td>Kabupaten*</td>
			<td><input type="text" name="kab" id="kab" class="form-control" placeholder="kabupaten"></td>
	</tr>
	<tr>
			<td>Telp*</td>
			<td>
				<input type="text" name="telp" id="telp" class="form-control" placeholder="no telp rumah">
			</td>
			<td>Hp*</td>
			<td>
				<input type="text" name="hp" id="hp" class="form-control" placeholder="no hp">
			</td>
	</tr>
	<tr>
			<td>Nama Ayah*</td>
			<td><input type="text" name="nmayah" id="nmayah" class="form-control" placeholder="nama ayah lengkap"></td>
	</tr>
	<tr>
			<td>Nama Ibu*</td>
			<td><input type="text" name="nmibu" id="nmibu" class="form-control" placeholder="nama ibu lengkap"></td>
	</tr>
	<tr>
			<td>Nama Ortu / Wali*</td>
			<td><input type="text" name="nmortuwali" id="nmortuwali" class="form-control" placeholder="nama ortu / wali lengkap"></td>
	</tr>
	<tr>
            <td>Pekerjaan Ortu / Wali</td>
    </tr>
	<tr>
			<td>Ayah*</td>
			<td>
				<input name="kerja" type="radio" value="guru" />Guru
                <input name="kerja" type="radio" value="petani" />Petani
				<input name="kerja" type="radio" value="pedagang" />Pedagang
				<input name="kerja" type="radio" value="wiraswasta" />Wiraswasta
				<input name="kerja" type="radio" value="lainnya" />Lainnya...
				<input name="kerja" type="radio" value="tidak ada" />Tidak Ada</br></br>
			</td>
	</tr>
	<tr>
			<td>Ibu*</td>
			<td>
				<input name="kerja2" type="radio" value="guru" />Guru
                <input name="kerja2" type="radio" value="petani" />Petani
				<input name="kerja2" type="radio" value="pedagang" />Pedagang
				<input name="kerja2" type="radio" value="wiraswasta" />Wiraswasta
				<input name="kerja2" type="radio" value="lainnya" />Lainnya...
				<input name="kerja2" type="radio" value="tidak ada" />Tidak Ada</br></br>
			</td>
	</tr>
	<tr>
			<td>Wali*</td>
			<td>
				<input name="kerja3" type="radio" value="guru" />Guru
                <input name="kerja3" type="radio" value="petani" />Petani
				<input name="kerja3" type="radio" value="pedagang" />Pedagang
				<input name="kerja3" type="radio" value="wiraswasta" />Wiraswasta
				<input name="kerja3" type="radio" value="lainnya" />Lainnya...
				<input name="kerja3" type="radio" value="tidak ada" />Tidak Ada</br></br>
			</td>
	</tr>
	<tr>
            <td>Pendidikan Ortu / Wali*</td>
	</tr>
	<tr>
			<td>Ayah*</td>
			<td>
                <input name="pnddkn" type="radio" value="sd" />SD
				<input name="pnddkn" type="radio" value="smp" />SMP
				<input name="pnddkn" type="radio" value="sma" />SMA
				<input name="pnddkn" type="radio" value="d3" />D3
				<input name="pnddkn" type="radio" value="s1" />S1
				<input name="pnddkn" type="radio" value="s2" />S2
				<input name="pnddkn" type="radio" value="lainnya" />Lainnya...
				<input name="pnddkn" type="radio" value="tidak ada" />Tidak Ada</br></br>				
			</td>
	</tr>
	<tr>
			<td>Ibu*</td>
			<td>
                <input name="pnddkn2" type="radio" value="sd" />SD
				<input name="pnddkn2" type="radio" value="smp" />SMP
				<input name="pnddkn2" type="radio" value="sma" />SMA
				<input name="pnddkn2" type="radio" value="d3" />D3
				<input name="pnddkn2" type="radio" value="s1" />S1
				<input name="pnddkn2" type="radio" value="s2" />S2
				<input name="pnddkn2" type="radio" value="lainnya" />Lainnya...
				<input name="pnddkn2" type="radio" value="tidak ada" />Tidak Ada</br></br>
			</td>
	</tr>
	<tr>
			<td>Wali*</td>
			<td>
                <input name="pnddkn3" type="radio" value="sd" />SD
				<input name="pnddkn3" type="radio" value="smp" />SMP
				<input name="pnddkn3" type="radio" value="sma" />SMA
				<input name="pnddkn3" type="radio" value="d3" />D3
				<input name="pnddkn3" type="radio" value="s1" />S1
				<input name="pnddkn3" type="radio" value="s2" />S2
				<input name="pnddkn3" type="radio" value="lainnya" />Lainnya...
				<input name="pnddkn3" type="radio" value="tidak ada" />Tidak Ada</br></br>
			</td>
    </tr>
	<tr>
			<td>Alamat*</td>
			<td><input type="text" name="almt" id="almt" class="form-control" placeholder="alamat ortu/wali lengkap"></br></td>
	</tr>
	<tr>
            <td>Status Anak*</td>
			<td><input name="sttsanak" type="radio" value="anak kandung" />Anak Kandung
                <input name="sttsanak" type="radio" value="anak tidak kandung" />Anak Tidak Kandung</br></br>
			</td>
    </tr>
	<tr>
            <td>Hobi*</td>
			<td>
				<input name="hobi" type="radio" value="olahraga" />Olahraga
                <input name="hobi" type="radio" value="membaca" />Membaca
				<input name="hobi" type="radio" value="menulis" />Menulis
				<input name="hobi" type="radio" value="kesenian" />Kesenian
				<input name="hobi" type="radio" value="travelling" />Travelling
				<input name="hobi" type="radio" value="lainnya" />Lainnya...</br></br>
			</td>
    </tr>
	<tr>
            <td>Cita-Cita*</td>
			<td>
				<input name="cita" type="radio" value="guru" />Guru
				<input name="cita" type="radio" value="dokter" />Dokter
				<input name="cita" type="radio" value="tni/polri" />TNI/Polri
				<input name="cita" type="radio" value="wirausaha" />Wirausaha
				<input name="cita" type="radio" value="seniman" />Seniman
				<input name="cita" type="radio" value="lainnya" />Lainnya...</br></br>
				
			</td>
    </tr>
	<tr>
			<td>Asal Sekolah*</td>
			<td><input type="text" name="asalsekolah" id="asalsekolah" class="form-control" placeholder="asal sekolah"></td>
	</tr>
	<tr>
			<td>Nama Sekolah*</td>
			<td><input type="text" name="nmsekolah" id="nmsekolah" class="form-control" placeholder="nama lengkap sekolah"></td>
	</tr>
	<div class="container">
	<tr>
		<td><center></br>
			<input name="submit" type="submit" value="DAFTAR" class="btn btn-primary" onclick="return konfirmregist()"/>
		</td>
		<td><center></br>
			<input name="reset" type="reset" value="BATAL" class="btn btn-danger"/>
		</td>
	</tr>
	</div>
	</table>
 </form>

 <?php
	// DB connection info
 $host = "localhost";
 $user = "root";
 $pass = "";
 $db = "psb_online";
 // Connect to database.
 try
 {
     $connect = new PDO( "mysql:host=$host;dbname=$db", $user, $pass);
     $connect->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
 }
 catch(Exception $e)
 {
     die(var_dump($e));
 }
	
	if(!empty($_POST)){
    try
    {
		$id = $_POST['id'];
		$tanggal = date("Y-m-d");
        $nama = $_POST['nama'];
		$jenis = $_POST['jenis'];
		$tempat = $_POST['tempat'];
		$tgl = $_POST['tgl'];
		$anak = $_POST['anak'];
		$jumlah = $_POST['jumlah'];
		$alamat = $_POST['alamat'];
		$rt = $_POST['rt'];
		$rw = $_POST['rw'];
		$keldes = $_POST['keldes'];
		$kodepos = $_POST['kodepos'];
		$kec = $_POST['kec'];
		$kab = $_POST['kab'];
		$telp = $_POST['telp'];
		$hp = $_POST['hp'];
		$nmayah = $_POST['nmayah'];
		$nmibu = $_POST['nmibu'];
		$nmortuwali = $_POST['nmortuwali'];
		$kerja = $_POST['kerja'];
		$kerja2 = $_POST['kerja2'];
		$kerja3 = $_POST['kerja3'];
		$pnddkn = $_POST['pnddkn'];
		$pnddkn2 = $_POST['pnddkn2'];
		$pnddkn3 = $_POST['pnddkn3'];
		$almt = $_POST['almt'];
		$sttsanak = $_POST['sttsanak'];
		$hobi = $_POST['hobi'];
		$cita = $_POST['cita'];
		$asalsekolah = $_POST['asalsekolah'];
		$nmsekolah = $_POST['nmsekolah'];
        // Insert data
        $sql_insert = "INSERT INTO pendaftaran (id, tanggal, nama, jenis, tempat, tgl, anak, jumlah, alamat, rt, rw, keldes, kodepos, kec, kab, telp, hp, nmayah, nmibu, nmortuwali, kerja,kerja2,kerja3, pnddkn, pnddkn2, pnddkn3, almt, sttsanak, hobi, cita, asalsekolah, nmsekolah) 
                        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $stmt = $connect->prepare($sql_insert);
		$stmt->bindValue(1, $id);
		$stmt->bindValue(2, $tanggal);
        $stmt->bindValue(3, $nama);
		$stmt->bindValue(4, $jenis);
		$stmt->bindValue(5, $tempat);
		$stmt->bindValue(6, $tgl);
		$stmt->bindValue(7, $anak);
		$stmt->bindValue(8, $jumlah);
		$stmt->bindValue(9, $alamat);
		$stmt->bindValue(10, $rt);
		$stmt->bindValue(11, $rw);
		$stmt->bindValue(12, $keldes);
		$stmt->bindValue(13, $kodepos);
		$stmt->bindValue(14, $kec);
		$stmt->bindValue(15, $kab);
		$stmt->bindValue(16, $telp);
		$stmt->bindValue(17, $hp);
		$stmt->bindValue(18, $nmayah);
		$stmt->bindValue(19, $nmibu);
		$stmt->bindValue(20, $nmortuwali);
		$stmt->bindValue(21, $kerja);
		$stmt->bindValue(22, $kerja2);
		$stmt->bindValue(23, $kerja3);
		$stmt->bindValue(24, $pnddkn);
		$stmt->bindValue(25, $pnddkn2);
		$stmt->bindValue(26, $pnddkn3);
		$stmt->bindValue(27, $almt);
		$stmt->bindValue(28, $sttsanak);
		$stmt->bindValue(29, $hobi);
		$stmt->bindValue(30, $cita);
        $stmt->bindValue(31, $asalsekolah);
		$stmt->bindValue(32, $nmsekolah);
        $stmt->execute();
    }
        catch(Exception $e)
        {
            die(var_dump($e));
        }
        echo "<script language='javascript'> alert('REGISTRASI Berhasil!..');
		document.location='daftarsiswa.php';
		</script>";
}
 ?>
      </div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <div class="col-md-10">
            	<h2> Lokasi </h2>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.1384323697266!2d110.30336911416737!3d-7.984636694249445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7b006d2e5b5d4b%3A0x3f4ffc0fd2a3fd6a!2sSD%20Unggulan%20Muhammadiyah%20Kretek!5e0!3m2!1sen!2sid!4v1568249868500!5m2!1sen!2sid" width="200" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
    </div>
    <div class="card">
      <h3>Popular Post</h3>
		<div class="slideshow-container">
		<div class="mySlides fade">
		  <div class="numbertext">1 / 10</div>
		  <img src="post1.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">2 / 10</div>
		  <img src="post2.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">3 / 10</div>
		  <img src="post3.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">4 / 10</div>
		  <img src="next4.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">5 / 10</div>
		  <img src="next5.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">6 / 10</div>
		  <img src="next6.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">7 / 10</div>
		  <img src="next7.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">8 / 10</div>
		  <img src="next8.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">9 / 10</div>
		  <img src="next10.jpg" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">10 / 10</div>
		  <img src="next11.jpg" style="width:100%">
		</div>
		</div>
		<br>

		<div style="text-align:center">
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		</div>

		<script>
		var slideIndex = 0;
		showSlides();
		function showSlides() {
			var i;
			var slides = document.getElementsByClassName("mySlides");
			var dots = document.getElementsByClassName("dot");
			for (i = 0; i < slides.length; i++) {
			   slides[i].style.display = "none";  
			}
			slideIndex++;
			if (slideIndex> slides.length) {slideIndex = 1}    
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex-1].style.display = "block";  
			dots[slideIndex-1].className += " active";
			setTimeout(showSlides, 2000); // Change image every 2 seconds
		}
		</script>
    </div>
  </div>
</div>
<div class="footer">
  <div class="row">
        	<div class="col-md-12 text-center">
            	copyright&copy<strong>SD Unggulan Muhammadiyah Kretek, 2019</strong>
            </div>
        </div>
</div>
</body>
</html>
