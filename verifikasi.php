<!DOCTYPE html>
<html>
<head>
<title>EDIT - SD Unggulan Muhammadiyah Kretek</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" 
	integrity="9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIh0egiu1Fw05qRGvFX0dJZ4" crossorigin="anonymous">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial;
  padding: 1px;
  background: #f1f1f1;
}

/* Header/Blog Title */
.header {
  padding: 30px;
  text-align: center;
  background: white;
  background-image: url("Header2.png");
}

.header h1 {
  font-size: 50px;
}

.menu {
    font-family:arial;
    font-weight:bold;
    width:980;
    border-radius:5px;
    font-size:14px;
	background:#222;
}
.menu ul {
    padding:0;
    margin:0;
}
.menu ul li {
    list-style:none;
    display:inline-block;
}
.menu ul li a {
    display:block;
    text-decoration:none;
    padding:20px;
    color:white;
}
.menu ul li a:hover {
    background-color:#ddd;
    color:black;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  background-color: #f1f1f1;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #f1f1f1;
  width: 100%;
  padding: 20px;
}

/* Add a card effect for articles */
.card {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background-color: white;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
  .topnav a {
    float: none;
    width: 100%;
  }
}

* {box-sizing:border-box}
body {font-family: Verdana,sans-serif;}
.mySlides {display:none}
/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}
/* Caption text */
.text {
  color: #f2f2f2;
  font-size: 15px;
  padding: 8px 12px;
  position: absolute;
  bottom: 8px;
  width: 100%;
  text-align: center;
}
/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}
/* The dots/bullets/indicators */
.dot {
  height: 13px;
  width: 13px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}
.active {
  background-color: #717171;
}
/* Fading animation */
.fade {
  -webkit-animation-name: fade;
  -webkit-animation-duration: 1.5s;
  animation-name: fade;
  animation-duration: 1.5s;
}
@-webkit-keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
@keyframes fade {
  from {opacity: .4} 
  to {opacity: 1}
}
/* On smaller screens, decrease text size */
@media only screen and (max-width: 300px) {
  .text {font-size: 11px}
}
</style>
</head>
<body>

<div class="header">
  <h1>HALAMAN ADMIN</h1>
  <h2>SD Unggulan Muhammadiyah Kretek</h2>
</div>

<nav class="menu">
    <ul>
		<li><a href="indexadmin.php">HOME</a></li>
        <li><a href="dataidentitasweb.php">Mengelola Data Identitas Website</a></li>
		<li><a href="informasipsb.php">Mengelola Informasi PSB</a></li>
    </ul>
</nav>
<div class="row">
  <div class="leftcolumn">
    <div class="card">
	<?php
	include 'koneksi.php';
	$id	= $_GET['id'];
	$sql ="select * from pendaftaran where id='$id'";
	$query=mysqli_query($koneksi, $sql);
	if(mysqli_num_rows($query)>0)
		{
	$data= mysqli_fetch_array($query);
		}
	?>
			
			<form method="post" action="vrfks.php">
				<input type="hidden" value="<?php echo $data['id'];?>" name="id">
				<table>
					<tr>
						<td>NISN</td>
						<td><input type="text" value="<?php echo $data['id'];?>" name="id"></td>
					</tr>
					<tr>
						<td>Nama</td>
						<td><input type="text" value="<?php echo $data['nama'];?>" name="nama"></td>
					</tr>
					<tr>
						<td>Verifikasi</td>
						<td>
							<input name="sttsverifikasi" type="radio" value="sudah" />Sudah
							<input name="sttsverifikasi" type="radio" value="belum" />Belum
						</td>
					</tr>
					<div class="container">
						<tr>
							<td><center></br>
								<input name="submit" type="submit" value="SIMPAN" class="btn btn-primary" onclick="return konfirmregist()"/>
							</td>
							<td><center></br>
								<a href="informasipsb.php" name="reset" type="reset" value="KEMBALI" class="btn btn-danger">KEMBALI</a>
							</td>
						</tr>
					</div>
				</table>
			</form>
	</div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <div class="col-md-10">
            	<h2> Lokasi </h2>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.1384323697266!2d110.30336911416737!3d-7.984636694249445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7b006d2e5b5d4b%3A0x3f4ffc0fd2a3fd6a!2sSD%20Unggulan%20Muhammadiyah%20Kretek!5e0!3m2!1sen!2sid!4v1568249868500!5m2!1sen!2sid" width="200" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
    </div>
    <div class="card">
      <h3>Popular Post</h3>
		<div class="slideshow-container">
		<div class="mySlides fade">
		  <div class="numbertext">1 / 10</div>
		  <img src="post1.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">2 / 10</div>
		  <img src="post2.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">3 / 10</div>
		  <img src="post3.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">4 / 10</div>
		  <img src="next4.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">5 / 10</div>
		  <img src="next5.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">6 / 10</div>
		  <img src="next6.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">7 / 10</div>
		  <img src="next7.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">8 / 10</div>
		  <img src="next8.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">9 / 10</div>
		  <img src="next10.JPG" style="width:100%">
		</div>
		<div class="mySlides fade">
		  <div class="numbertext">10 / 10</div>
		  <img src="next11.JPG" style="width:100%">
		</div>
		</div>
		<br>

		<div style="text-align:center">
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		  <span class="dot"></span> 
		</div>

		<script>
		var slideIndex = 0;
		showSlides();
		function showSlides() {
			var i;
			var slides = document.getElementsByClassName("mySlides");
			var dots = document.getElementsByClassName("dot");
			for (i = 0; i < slides.length; i++) {
			   slides[i].style.display = "none";  
			}
			slideIndex++;
			if (slideIndex> slides.length) {slideIndex = 1}    
			for (i = 0; i < dots.length; i++) {
				dots[i].className = dots[i].className.replace(" active", "");
			}
			slides[slideIndex-1].style.display = "block";  
			dots[slideIndex-1].className += " active";
			setTimeout(showSlides, 2000); // Change image every 2 seconds
		}
		</script>
    </div>
  </div>
</div>

<div class="footer">
  <div class="row">
        	<div class="col-md-12 text-center">
            	copyright&copy<strong>SD Unggulan Muhammadiyah Kretek, 2019</strong>
            </div>
        </div>
</div>

</body>
</html>