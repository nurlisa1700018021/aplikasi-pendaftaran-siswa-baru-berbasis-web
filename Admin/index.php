<!DOCTYPE html>
<html lang="en">
<head>
  <title>LOGIN - SD Unggulan Muhammadiyah Kretek</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <center><div class="container">           
	  <img src="log.png" width="25%" class="rounded-circle img-thumbnail"> 
	</div></center>
  <form action="proseslogin.php" method="post" class="was-validated">
    <div class="form-group">
      <label for="txtuname">Username :</label>
      <input type="text" class="form-control" id="uname" placeholder="Enter username" name="txtuname" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Silahkan isi kolom ini.</div>
    </div>
    <div class="form-group">
      <label for="txtpwd">Password :</label>
      <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="txtpwd" required>
      <div class="valid-feedback">Valid.</div>
      <div class="invalid-feedback">Silahkan isi kolom ini.</div>
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember" required> Saya setuju.
        <div class="valid-feedback">Valid.</div>
        <div class="invalid-feedback">Centang kotak ini untuk melanjutkan.</div>
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
</body>
</html>
