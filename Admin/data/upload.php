<!DOCTYPE html>
<html>
<head>
<title>UPLOAD SUKSES - SD Unggulan Muhammadiyah Kretek</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" 
	integrity="9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIh0egiu1Fw05qRGvFX0dJZ4" crossorigin="anonymous">
<style>
* {
  box-sizing: border-box;
}

body {
  font-family: Arial;
  padding: 10px;
  background: #f1f1f1;
}

/* Header/Blog Title */
.header {
  padding: 30px;
  text-align: center;
  background-image: url("../Header2.png");
}

.header h1 {
  font-size: 50px;
}

/* Create two unequal columns that floats next to each other */
/* Left column */
.leftcolumn {   
  float: left;
  width: 75%;
}

/* Right column */
.rightcolumn {
  float: left;
  width: 25%;
  background-color: #f1f1f1;
  padding-left: 20px;
}

/* Fake image */
.fakeimg {
  background-color: #f1f1f1;
  width: 100%;
  padding: 20px;
}

/* Add a card effect for articles */
.card {
  background-color: white;
  padding: 20px;
  margin-top: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Footer */
.footer {
  padding: 20px;
  text-align: center;
  background-color: white;
  margin-top: 20px;
}

/* Responsive layout - when the screen is less than 800px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 800px) {
  .leftcolumn, .rightcolumn {   
    width: 100%;
    padding: 0;
  }
}

/* Responsive layout - when the screen is less than 400px wide, make the navigation links stack on top of each other instead of next to each other */
@media screen and (max-width: 400px) {
  .topnav a {
    float: none;
    width: 100%;
  }
}
</style>
</head>
<body>

<div class="header">
  <h1>PENDAFTARAN SISWA BARU</h1>
  <h2>SD Unggulan Muhammadiyah Kretek</h2>
</div>
<div class="row">
  <div class="leftcolumn">
    <div class="card">
	<center></br></br>
	<?php
	$folder="../data/";
	$namafile=$_FILES['txtfile']['name'];
	$uploadfile=$folder.$namafile;
	$tmp_name=$_FILES['txtfile']['tmp_name'];
	$ukuran=$_FILES['txtfile']['size'];
	$jenis=$_FILES['txtfile']['type'];
	include("../koneksi.php");
	$sql="select count(*) as jum from upload where name='$namafile'";
	$hasil=mysqli_query($koneksi,$sql);
	$data=mysqli_fetch_array($hasil);
	if($data['jum']>0)
	{
		$sql="update upload set size='$ukuran' where name='$namafile'";
		}
	else
	{
		$sql="insert into upload (name,type,size) values ('$namafile','$jenis','$ukuran')";
		}
	mysqli_query($koneksi,$sql);
	if(move_uploaded_file($_FILES['txtfile']['tmp_name'],$uploadfile))
	{
		echo "<strong>File Dokumen sesuai dan BERHASIL diupload !!!</strong>";
		}
	else
	{
		echo "<strong>File Dokumen tidak sesuai dan GAGAL diupload ???</strong>";
		}
	?>
	<form action="../../index.php" method="post" enctype="multipart/form-data">
        <input type="submit" value="Kembali Ke Halaman Utama" class="btn btn-primary" name="submit" />
    </form>
	</br></br></br></center>
	</div>
  </div>
  <div class="rightcolumn">
    <div class="card">
      <div class="col-md-10">
            	<h2> Lokasi </h2>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3951.1384323697266!2d110.30336911416737!3d-7.984636694249445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7b006d2e5b5d4b%3A0x3f4ffc0fd2a3fd6a!2sSD%20Unggulan%20Muhammadiyah%20Kretek!5e0!3m2!1sen!2sid!4v1568249868500!5m2!1sen!2sid" width="200" height="200" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>
    </div>
  </div>
</div>

<div class="footer">
  <div class="row">
        	<div class="col-md-12 text-center">
            	copyright&copy SD Unggulan Muhammadiyah Kretek, 2019
            </div>
        </div>
</div>

</body>
</html>
